# My [Curriculum Vitae](https://en.wikipedia.org/wiki/Curriculum_vitae)

Currently done in LaTeX on [Overleaf](https://www.overleaf.com/).

Different branches or tags signal different versions.

Check out my [LinkedIn](https://linkedin.com/in/bibin-muttappillil-598433244/) as well.

Coming ["soon"](https://developer.valvesoftware.com/wiki/Valve_Time): my website [bibin.ch](https://bibin.ch)
